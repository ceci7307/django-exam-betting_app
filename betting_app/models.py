from django.db import models, transaction
from django.contrib.auth.models import User
from decimal import Decimal
from django.db.models.query import QuerySet
from django.conf import settings
from uuid import uuid4


class UserProfile (models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.IntegerField(default = 0)
    role = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, user):
        userprofile = cls()
        userprofile.user = user
        userprofile.save()

    def __str__(self):
        return f"{self.user}"

class UserBankDetail (models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    card_number = models.CharField(max_length = 16, default = None)
    expire_date = models.DateField(auto_now_add=False)
    cvc = models.CharField(max_length = 3, default = None)

    def __str__(self):
        return f'{self.user}'


class Account (models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    account_number = models.CharField(max_length=36)

    @property
    def balance(self) -> Decimal:
        return Ledger.objects.filter(account=self).aggregate(models.Sum('amount'))['amount__sum'] or Decimal('0')

    def __str__(self):
            return f'{self.user}'
        

class Ledger (models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)


    @classmethod
    def transfer(self, account_number, amount):
        self(account=account_number, amount=amount).save()

    def __str__(self):
        return f'{self.account}:{self.amount}'


class Game (models.Model):
    leauge = models.CharField(max_length = 200, default = None)
    home_team_name = models.CharField(max_length = 200, default = None)
    away_team_name = models.CharField(max_length = 200, default = None)
    draw = models.CharField(max_length = 200, default = None)
    match_date = models.DateField(auto_now_add=False)
    home_odds_number = models.DecimalField(max_digits=10, decimal_places=2)
    away_odds_number = models.DecimalField(max_digits=10, decimal_places=2)
    draw_odds_number = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f'{self.pk}:{self.home_team_name}:{self.away_team_name}'

class Bet (models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    game = models.ForeignKey(Game, on_delete=models.PROTECT)
    selected_team = models.CharField(max_length = 200, default = None)
    bet_amount = models.DecimalField(max_digits=10, decimal_places=2)

    @classmethod
    def create(cls, user, game, selected_team, bet_amount):
        bet = cls()
        bet.user = user 
        bet.game = game
        bet.selected_team = selected_team
        bet.bet_amount = bet_amount
        bet.save()

    def __str__(self):
        return f'{self.user}:{self.selected_team}:{self.bet_amount}:{self.game}'

class Winner (models.Model):
    game = models.OneToOneField(Game, on_delete=models.PROTECT)
    winner = models.CharField(max_length = 200, default = None)

    @classmethod
    def create(cls, game):
        winner = cls()
        winner.game = game
        winner.save() 

    def __str__(self):
        return f'{self.game}:{self.winner}'
