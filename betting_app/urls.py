from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls import url
from .api import AllMyBets, MyBet

urlpatterns = [
    path('', views.frontpage, name='frontpage'),
    path('logout/', views.logout, name='logout'),
    path('test/', views.test, name='test'),
    path('betting-admin/', views.bettingAdmin, name='bettingAdmin'),
    path('create-bet/', views.createBet, name='createBet'),
    path('singleview/<int:pk>/', views.singleview, name='singleview'),
    path('betting-user/', views.bettingUser, name='bettingUser'),
    path('signup/', views.signup, name='signup'),
    path('userdetail/', views.userdetail, name='userdetail'),
    path('update_password/', views.update_password, name='update_password'),
    path('update_userdetails/', views.update_userdetails, name='update_userdetails'),
    path('update_winner/', views.update_winner, name='update_winner'),
    path('update_role/', views.update_role, name='update_role'),
    path('place_bet/', views.place_bet, name='place_bet'),
    path('banktransfer/', views.BankTransfer, name='BankTransfer'),
    path('api/v1/', AllMyBets.as_view()),
    path('api/v1/<int:pk>/', MyBet.as_view()),
    path('api/v1/rest-auth/', include('rest_auth.urls')),
    path('payout/', views.payout, name='payout'),
]