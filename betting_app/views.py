from django.shortcuts import render, get_object_or_404, reverse
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from . import models
from .models import UserProfile, Account, Ledger, UserBankDetail, Game, Winner, Bet
from decimal import Decimal
import random
import django_rq
from . messaging import email_message

def test(request):
    context = {}
    return render( request, 'betting_app/test.html', context)

def randomGen():
    # return a 6 digit random number
    return int(random.uniform(100000, 999999))

def signup(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        first_name = request.POST['firstname']
        last_name = request.POST['lastname']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        email = request.POST['email']

        phone = request.POST['phone']
        role = 'customer'

        if password == confirm_password:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            if user:
                userprofile = UserProfile()
                userprofile.user = user
                userprofile.phone = phone
                userprofile.role = role
                userprofile.save()
                return HttpResponseRedirect(reverse('bettingUser'))
            else:
                context = {'error': 'Could not create user - try something else'}
        else:
            context ={'error': 'Passwords do not match.'}

    return render( request, 'betting_app/signup.html', context)

def bettingAdmin(request):
    if request.user.is_authenticated:
        user = request.user
        if user.userprofile.role == 'admin':
            context = {
                'games' : Game.objects.all(),
                'winners': Winner.objects.all(),
                'users': User.objects.all(),
                'curr_users': Account.objects.get(user=request.user),
                'ledgers': Ledger.objects.all()
                #'all_bets_count': Bet.objects.filter(user=request.user).count(),
            }
            return render( request, 'betting_app/betting-admin.html', context)
        else:
            return HttpResponseRedirect(reverse('frontpage'))
    else:
        return HttpResponseRedirect(reverse('frontpage'))


def update_winner(request):
    if request.method == "POST":
        pk = request.POST['pk']
        game = Game.objects.get(pk=pk)
        winner = get_object_or_404(Winner, game=game)
        winner_data = request.POST['options']
        if game:
            winner.winner = winner_data
            winner.save()

    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def createBet(request):
    context = {}
    if request.method == 'POST':
        leauge = request.POST['leauge']
        home_team_name = request.POST['home_team_name']
        draw = request.POST['draw']
        away_team_name = request.POST['away_team_name']
        match_date = request.POST['match_date']
        home_odds_number = request.POST['home_odds_number']
        away_odds_number = request.POST['away_odds_number']
        draw_odds_number = request.POST['draw_odds_number']

        game = Game()
        game.leauge = leauge
        game.home_team_name = home_team_name
        game.away_team_name = away_team_name
        game.draw = draw
        game.match_date = match_date
        game.home_odds_number = home_odds_number
        game.away_odds_number = away_odds_number
        game.draw_odds_number = draw_odds_number
        game.save()

        if game:
            winner = Winner()
            winner.game = game
            winner.winner = 'Not settled'
            winner.save()

    return render( request, 'betting_app/admin-create-bet.html', context)

def singleview(request, pk):
    user = User.objects.get(pk=pk)
    context = {
        'user':user,
    }
    return render( request, 'betting_app/customer-singleview.html', context)

def update_role(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        userprofile = get_object_or_404(UserProfile, user=user)
        role = request.POST['role']
        if user:
            userprofile.role = role
            userprofile.save()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])


def bettingUser(request):
    if request.user.is_authenticated:

        userBets = Bet.objects.all().filter(user=request.user)
        latestGames = Game.objects.all().order_by('-id')
        latestGame = latestGames[0]
        secondLatest = latestGames[1]

        try:
            curr_user = Account.objects.get(user=request.user)
        except:
            curr_user = Account()
            curr_user.account_number = randomGen() 
            curr_user.user = request.user
            #curr_user.account_number(request.user, request.POST['balance'])
            curr_user.save() 
        

        context = {
        'curr_user': curr_user,
        'games' : Game.objects.all(),
        'winners': Winner.objects.all(),
        'userBets': userBets,
        'latestGame': latestGame,
        'secondLatest': secondLatest,
        }
        return render( request, 'betting_app/betting-user.html', context)
    else:
        return HttpResponseRedirect(reverse('frontpage'))

def place_bet(request):
    if request.method == "POST":
        game_pk = request.POST['game_pk']
        game = Game.objects.get(pk=game_pk)
        bet_amount = request.POST['amount']
        selected_team = request.POST['selected_team']
        Bet.create(request.user, game, selected_team, bet_amount)

    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def userdetail(request):
    context = {}
    return render( request, 'betting_app/userdetail.html', context)

def update_userdetails(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        userprofile = get_object_or_404(UserProfile, user=user)
        phone = request.POST['phonenumber']
        email = request.POST['email']
        
        if user:
            user.email = email
            user.save()
            userprofile.phone = phone
            userprofile.save()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def update_password(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        
        if user:
            if password == confirm_password:
                user.set_password(password)
                user.save()
    return HttpResponseRedirect(reverse('frontpage'))


def payout(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        money = request.POST['money']
        
        if user:
            django_rq.enqueue(email_message, {
               'money' : money,
               'email_receiver' : user.email,
            })
            reset_amount = Ledger.objects.get(account=user.account)
            reset_amount.amount = '0'
            reset_amount.save()
            return HttpResponseRedirect(reverse('bettingUser'))

    return render(request, 'betting_app/betting-user.html')


def BankTransfer(request):
    context = {}
    #try: 
    #    banktransfer = UserBankDetail.objects.get(user=request.user)
   # except: 
    if request.method == 'POST':
                card_number = request.POST['card_number']
                cvc = request.POST['cvc']
                expire_date = request.POST['expire_date']
                amount = (request.POST['amount'])
                account_number = get_object_or_404(Account, user=request.user)
                Ledger.transfer(account_number, amount)
                
                try:
                    bankTransfer = UserBankDetail.objects.get(user=request.user)
                    if bankTransfer:
                        bankTransfer.card_number = card_number
                        bankTransfer.cvc = cvc
                        bankTransfer.expire_date = expire_date
                        bankTransfer.save()
                except:
                    bankTransfer = UserBankDetail()
                    bankTransfer.user = request.user
                    bankTransfer.card_number = card_number
                    bankTransfer.cvc = cvc
                    bankTransfer.expire_date = expire_date
                    bankTransfer.save()
                
                return HttpResponseRedirect(reverse('bettingUser'))
   # else:
   #     return render(request, 'betting_app/Banktransfer-form.html', context)

  

def frontpage(request):
    context = {
        'games' : Game.objects.all(),
    }
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            if user.userprofile.role == 'admin':
                dj_login(request, user)
                return HttpResponseRedirect(reverse('bettingAdmin'))
            else:
                dj_login(request, user)
                return HttpResponseRedirect(reverse('bettingUser'))
        else:
            context = {'error': 'Bad username or password.'}

    return render( request, 'betting_app/frontpage.html', context)

def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('frontpage'))
